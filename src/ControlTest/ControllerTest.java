package ControlTest;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import FrameTest.FrameTesting;
import Model.Hashing_funct;


public class ControllerTest {
	private static final int FRAME_WIDTH = 600;
	private static final int FRAME_HEIGHT = 500;
	public FrameTesting frame;
	ActionListener list;
	public String HeadStr ="\tURL\t\tHash-Code\n";
	public String[]urlTester = {"http://www.deviantart.com" ,
						  		"http://www.garena.in.th" ,
								"http://www.soul-anime.tv" ,
								"https://twitter.com" ,
								"https://www.youtube.com" ,
							    "http://www.thaiboyslove.com" ,
								"http://www.dek-d.com" ,
								"http://www.tunwalai.com" ,
								"http://www.pixiv.net" ,
								"http://www.mangago.me" ,
								"https://www.tumblr.com" ,
								"https://www.dailymotion.com"};
	
	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);
			
		}  
	 }
	
	public static void main(String[] args) {
		new ControllerTest();
    }
	
	public ControllerTest() {
		frame = new FrameTesting();
		frame.pack();
		frame.setVisible(true);
		frame.setBounds(300,100,FRAME_WIDTH,FRAME_HEIGHT);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}
	
	public void setTestCase() {
		Hashing_funct hashModel = new Hashing_funct();
		frame.setResult(HeadStr);  //head
		int i;
		for(i=0;i<urlTester.length;i++){
			int sum = hashModel.chengeAscii(urlTester[i]);
			int code = hashModel.hashingMod(sum);
			frame.extendResult(urlTester[i]+"\t\t Hash-code : "+code);
		}
	}
}
