package FrameTest;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class FrameTesting extends JFrame 
{
	private JLabel showProjectName;
	private JTextArea showResults;
	private JButton endButton;
	private String str;
	
	
	public FrameTesting() {
		   createFrame();
		   
	   }
	   public void createFrame() {
		   showProjectName = new JLabel("Test : Hashing Function");
		   showResults = new JTextArea("\tURL\t\t\t\tHash-Code\n");
		   showResults.setEditable(false);
		   
		   endButton = new JButton("End program");
		   setLayout(new BorderLayout());
		   add(showProjectName, BorderLayout.NORTH);
		   add(showResults, BorderLayout.CENTER);
		   add(endButton, BorderLayout.SOUTH);
		   
	   }
	   
	   public void setProjectName(String s) {
		   showProjectName.setText(s);
	   }
	   
	   
	   public void setResult(String str) {
	       this.str = str;
		   showResults.setText(str);

	   }
	   
	   public void extendResult(String str) {
		   this.str = this.str+"\n"+str;
		   showResults.setText(this.str);
	   }
	   
	   
	   public void setListener(ActionListener list) {
		   endButton.addActionListener(list);
	   }
	   
		

}
